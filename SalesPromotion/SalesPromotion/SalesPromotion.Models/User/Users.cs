﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SalesPromotion.Models.User
{
    public class Users
    {
        public int Id { get; set; }

        // Resolving concurrency conflicts
        // https://docs.microsoft.com/en-us/ef/core/saving/concurrency
        [ConcurrencyCheck]
        public String Name { get; set; }
    }
}
