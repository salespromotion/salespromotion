﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesPromotion.Models.User
{
    public class Credentials
    {
        public string Name { get; set; }

        public string Age { get; set; }

        public string Sex { get; set; }

        public int Tel { get; set; }

        public int BankAcount { get; set; }

        public string Address { get; set; }

        public string PageInfor { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public int Role { get; set; }

        public int BalanceBank { get; set; }

        public int LevelPositive { get; set; }

        public int LevelDanger { get; set; }
    }
}
