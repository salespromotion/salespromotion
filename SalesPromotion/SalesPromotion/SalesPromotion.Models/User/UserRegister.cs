﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesPromotion.Models.User
{
    public class UserRegister
    {
        public string AcountName { get; set; }

        public string Password { get; set; }

        public int Telephone { get; set; }

        public string Email { get; set; }
    }
}
