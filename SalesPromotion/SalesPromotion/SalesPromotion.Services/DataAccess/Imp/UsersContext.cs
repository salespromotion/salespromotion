﻿using Microsoft.EntityFrameworkCore;
using SalesPromotion.Models.User;

namespace SalesPromotion.Services.DataAccess.Imp
{
    public class UsersContext : DbContext
    {
        public UsersContext(DbContextOptions<UsersContext> options) : base(options)
        {}

        public DbSet<Users> Users { get; set; }

    }
}
