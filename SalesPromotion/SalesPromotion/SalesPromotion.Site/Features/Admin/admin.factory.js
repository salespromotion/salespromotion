﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('admin', admin);

    admin.$inject = ['$http'];

    function admin($http) {
        var service = {
            getData: getData
        };

        return service;

        function getData() { }
    }
})();