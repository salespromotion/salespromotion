﻿(function () {
    'use strict';

    angular
        .module('app')
        .directive('Admin', Admin);

    Admin.$inject = ['$window'];

    function Admin($window) {
        // Usage:
        //     <Admin></Admin>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA'
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();