﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('AdminCtrl', AdminCtrl);

    AdminCtrl.$inject = ['$location'];

    function AdminCtrl($location) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'AdminCtrl';

        activate();

        function activate() { }
    }
})();
