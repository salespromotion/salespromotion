﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('GetUserLoginCtrl', GetUserLoginCtrl);

    GetUserLoginCtrl.$inject = ['$location', 'getUserLogin'];

    function GetUserLoginCtrl($location, getUserLogin) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'GetUserLoginCtrl';

        activate();

        function activate() { }
    }
})();
