﻿using Microsoft.AspNetCore.Mvc;
using SalesPromotion.Services.DataAccess.Imp;
using System.Linq;

namespace SalesPromotion.Site.Features.Home
{
    public class HomeController : Controller
    {
        private UsersContext context;

        public HomeController(UsersContext context) {
            this.context = context;
        }

        public IActionResult Index()
        {
            var dt = context.Users.ToList();
            return View();
        }
    }
}
