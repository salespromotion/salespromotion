﻿namespace SalesPromotion.Site.Core.Model
{
    public class NameEntity : BaseEntity
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return $"{Name} ({Id})";
        }
    }
}
