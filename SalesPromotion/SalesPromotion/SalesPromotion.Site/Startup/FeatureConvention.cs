﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;
using System.Linq;
using System.Reflection;

namespace SalesPromotion.Site.Startup
{
    public class FeatureConvention : IControllerModelConvention
    {
        public void Apply(ControllerModel controller)
        {
            controller.Properties.Add("feature", GetFeatureName(controller.ControllerType));
        }

        private String GetFeatureName(TypeInfo controllerType)
        {
            string[] tokens = controllerType.FullName.Split('.');
            if (!tokens.Any(t => t == "Features")) return "";
            string featureName = tokens
                .SkipWhile(t => !t.Equals("features",
                StringComparison.CurrentCultureIgnoreCase))
                .Skip(1)
                .Skip(1)
                .FirstOrDefault();
            return featureName;
        }
    }
}
