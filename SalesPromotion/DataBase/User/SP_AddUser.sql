USE [SP_V1]
GO

/****** Object:  StoredProcedure [dbo].[uspAddUser]    Script Date: 2017/05/08 19:04:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		MDTUAN 
-- Create date: 2017/05/08 
-- Description:	Create SP for add User
-- =============================================

CREATE PROCEDURE [dbo].[uspAddUser]
    @pLoginName NVARCHAR(50), 
    @pPassword NVARCHAR(50), 
	@pLevelMode tinyint = 1,
	@pBlackList bit = false,
    @responseMessage NVARCHAR(250) OUTPUT
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @salt UNIQUEIDENTIFIER=NEWID()

	IF EXISTS (SELECT TOP 1 UserID FROM [dbo].[User] WHERE LoginName=@pLoginName)
	SET @responseMessage='Invalid LoginName'
	RETURN -1

    BEGIN TRY

        INSERT INTO dbo.[User] (LoginName, PasswordHash,  Salt, LevelMode, BlackList)
        VALUES(@pLoginName, HASHBYTES('SHA2_512', @pPassword+CAST(@salt AS NVARCHAR(36))), @salt, @pLevelMode, @pBlackList)

        SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END
GO

