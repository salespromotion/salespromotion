USE [SalesPromotion]
GO

/****** Object:  Table [dbo].[GroupMenu]    Script Date: 4/29/2017 6:34:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GroupMenu](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[TypeGroup] [tinyint] NOT NULL,
	[Description] [nvarchar](150) NULL,
 CONSTRAINT [PK_GroupMenu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

