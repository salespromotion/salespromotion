USE [SalesPromotion]
GO

INSERT INTO [dbo].[News]
           ([Title]
           ,[Description]
           ,[Detail]
           ,[LevelPriority]
           ,[CreateDate]
           ,[UpdateDate]
           ,[ValidDate]
           ,[UserCreate]
           ,[UserUpdate]
           ,[UserReview]
           ,[TypeCategory]
           ,[DeleteFlag])
     VALUES
           (<Title, nvarchar(250),>
           ,<Description, nvarchar(250),>
           ,<Detail, nvarchar(250),>
           ,<LevelPriority, tinyint,>
           ,<CreateDate, datetime,>
           ,<UpdateDate, datetime,>
           ,<ValidDate, datetime,>
           ,<UserCreate, int,>
           ,<UserUpdate, int,>
           ,<UserReview, int,>
           ,<TypeCategory, int,>
           ,<DeleteFlag, bit,>)
GO

