USE [SalesPromotion]
GO

UPDATE [dbo].[News]
   SET [Title] = <Title, nvarchar(250),>
      ,[Description] = <Description, nvarchar(250),>
      ,[Detail] = <Detail, nvarchar(250),>
      ,[LevelPriority] = <LevelPriority, tinyint,>
      ,[CreateDate] = <CreateDate, datetime,>
      ,[UpdateDate] = <UpdateDate, datetime,>
      ,[ValidDate] = <ValidDate, datetime,>
      ,[UserCreate] = <UserCreate, int,>
      ,[UserUpdate] = <UserUpdate, int,>
      ,[UserReview] = <UserReview, int,>
      ,[TypeCategory] = <TypeCategory, int,>
      ,[DeleteFlag] = <DeleteFlag, bit,>
 WHERE <Search Conditions,,>
GO

