USE [SalesPromotion]
GO

INSERT INTO [dbo].[Privilege]
           ([Name]
           ,[Description]
           ,[DeleteFlag])
     VALUES
           (<Name, nchar(50),>
           ,<Description, nvarchar(150),>
           ,<DeleteFlag, bit,>)
GO

